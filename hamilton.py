import re
import os

def replace_images(string, replacement_value):
    image_line = re.sub("\S+.jpg", replacement_value, string)
    return image_line

def remove_spaces(string, separator):
    separated_string = re.sub(" {2,}", separator, string)
    return separated_string


def insert_tab(string, *args):
    string_to_list = list(string)
    for arg in args:
        string_to_list.insert(arg, "\t")
    list_to_string = "".join(string_to_list)
    return list_to_string

def process(file_to_open, output_file_name):
    f = open(file_to_open)
    path = os.path.split(file_to_open)[0]
    new_f = open(os.path.join(path, output_file_name), "w")
    for line in f:
        header_line = re.search("010111", line)
        if header_line:
            third = insert_tab(line, 43, 83)
            no_images = replace_images(third, "")
            final = remove_spaces(no_images, "")
            new_f.write(final)
        else:
            nh_no_tabs = insert_tab(line, 18, 49, 68, 83)
            nh_final = remove_spaces(nh_no_tabs, "")
            
            new_f.write(nh_final)
        
    new_f.close()
