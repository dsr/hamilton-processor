import tkinter
#from tkinter import *
import tkinter.ttk
import tkinter.filedialog
import tkinter.messagebox
from hamilton import process

def run():
    file_to_process = tkinter.filedialog.askopenfilename()
    if file_to_process:
        process(file_to_process, 'Hamilton_processed.txt')
        tkinter.messagebox.showinfo(message = 'All Done')
    else:
        pass

"""
BEGIN GUI

"""
root = tkinter.Tk()
root.resizable(tkinter.FALSE, tkinter.FALSE)
root.title('Hamilton Processor')
mainframe = tkinter.ttk.Frame(root, padding = "3 3 12 12")
mainframe.grid(column = 0, row = 0, sticky = (tkinter.N, tkinter.W, tkinter.E, tkinter.S))
mainframe.columnconfigure(0, weight = 1)
mainframe.rowconfigure(0, weight = 1)

tkinter.ttk.Label(mainframe, text = "Select the Hamilton text file").grid( \
        column = 1, row = 1)

tkinter.ttk.Button(mainframe, text = 'Select', command = run).grid( \
        column = 3, row = 1)

for child in mainframe.winfo_children():
    child.grid_configure(padx=5, pady=5)

root.mainloop()
